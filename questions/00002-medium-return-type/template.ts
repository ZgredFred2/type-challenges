// Type 'T' does not satisfy the constraint '(...args: any) => any'.
// type MyReturnType<T> = ReturnType<T>;
type MyReturnType<T> = T extends (...args: any) => infer R ? R : never;
