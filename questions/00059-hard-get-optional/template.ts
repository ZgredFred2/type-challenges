// type GetOptional<T> = Omit<T, keyof GetRequired<T>>;
type GetOptional<T> = {
  [P in keyof T as Omit<T, P> extends T ? P : never]: T[P];
};
