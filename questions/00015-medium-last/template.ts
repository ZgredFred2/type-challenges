// type Last<T extends any[]> = T extends [infer F, ...infer REST]
//   ? REST extends [any, ...any]
//   ? Last<REST>
//   : F
//   : never;

// type Last<T extends any[]> = T extends [infer F, ...infer R]
//   ? R["length"] extends 0
//   ? F
//   : Last<R>
//   : never;

// type Last<T extends any[]> = T extends [...any, infer A] ? A : never;

// type Last<T extends unknown[]> = [1, ...T][T["length"]];
type Last<T extends any[]> = T extends [infer head, ...infer rest]
  ? rest extends []
  ? head
  : Last<rest>
  : never;

// type X1 = Last<[3, 2, 1]>;
// type X2 = [() => 123, { a: string }];
// type X3 = [] extends [any, ...any] ? string : never;
