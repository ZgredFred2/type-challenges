import type { Equal } from "@type-challenges/utils";

export type GetReadonlyKeys<T> = keyof {
  [P in keyof T as Equal<Pick<T, P>, Readonly<Pick<T, P>>> extends true
    ? P
    : never]: T[P];
};
