type UnionToIntersection<U> = (
  U extends unknown ? (k: U) => void : never
) extends (k: infer I) => void
  ? I
  : never;

type X1 = UnionToIntersection<"foo" | 42 | true>;
type X3 = UnionToIntersection<(() => "foo") | ((i: 42) => true)>;
