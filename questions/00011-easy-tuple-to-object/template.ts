type TupleToObject<T extends readonly (keyof any)[]> = {
  [P in T[number]]: P;
};

type X = keyof any; // string | number | symbol
