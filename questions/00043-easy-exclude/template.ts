// type MyExclude<T, U> = keyof {
//   [P in T as P extends U ? never : P]: P;
// };
type MyExclude<T, U> = T extends U ? never : T;
type X2 = MyExclude<"A" | "B" | "C", "A" | "B">;

type T1 = "a" | "b" | "d" extends "a" | "b" | "c" ? true : false;
type T2 = "a" | "b" extends "a" | "b" | "c" ? true : false;
