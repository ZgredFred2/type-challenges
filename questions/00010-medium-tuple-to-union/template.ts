// type TupleToUnion<T extends { readonly [P in keyof T]: T[P] }> = T[keyof T];
type TupleToUnion<T extends unknown[]> = T extends (infer B)[] ? B : never;

// type TupleToUnion<T extends any[]> = T[number];

// least magicall solution
// type TupleToUnion<T extends readonly any[]> = T extends [infer F, ...infer R]
//   ? F | TupleToUnion<R>
//   : never;
