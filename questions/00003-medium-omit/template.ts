// type Options = "abc" | "cde" | "efg";
// type ObjectWithKeys = {
//   param1: string;
//   param2: string;
// };
// type Keys = keyof ObjectWithKeys;
// type NewObject = {
//   [Prop in Keys]: string;

// }
// type MyOmit<T, K> = {
//   [Prop in keyof T as item extends K ? never : item]: T[Prop];
// };
type MyOmit<T, K extends keyof T> = {
  [Prop in keyof T as Prop extends K ? never : Prop]: T[Prop];
};

// type MyMap<T> = {
//   [P in keyof T as `${P}_${T[P]}`]: T[P];
// };

// type MyType = MyMap<{ a: string; b: number }>;
// type MyType is now: { "a_string": string; "b_number": number; }

// type X = MyOmit<string, "toString">;

// type X = MyOmit<{ a: string; b: number }, "b">;
