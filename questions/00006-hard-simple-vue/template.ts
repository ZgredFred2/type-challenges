// https://www.typescriptlang.org/docs/handbook/utility-types.html#thistypetype
// https://github.com/typescript-eslint/typescript-eslint/blob/main/packages/eslint-plugin/docs/rules/ban-types.md
type InferredReturnType<T> = {
  [P in keyof T]: T[P] extends () => infer R ? R : never;
};
// type X = (() => any) extends (...args: any) => any ? boolean : never;
// type X = any extends any ? boolean : never;
// const x: X = true;
type ObjectDescriptor<D, C extends { [P in keyof C]: Function }, M> = {
  data?: () => D;
  // this or this?
  computed?: C & ThisType<D>;
  // computed?: C & ThisType<D & InferredReturnType<C>>;
  methods?: M & ThisType<D & InferredReturnType<C> & M>;
};
declare function SimpleVue<D, C extends { [P in keyof C]: Function }, M>(
  options: ObjectDescriptor<D, C, M>
): any;

// https://github.com/type-challenges/type-challenges/issues/20969
// type RecordFunction = Record<string, Function>;
// type TransitionComputed<T extends RecordFunction> = {
//   [P in keyof T]: T[P] extends () => infer A ? A : never;
// };
// type Vue<
//   Data extends Record<string, any>,
//   Computed extends RecordFunction,
//   Methods extends RecordFunction
// > = {
//   data?(): Data;
//   computed?: Computed & ThisType<Methods & TransitionComputed<Computed> & Data>;
//   methods?: Methods & ThisType<Methods & TransitionComputed<Computed> & Data>;
// };
//
// declare function SimpleVue<
//   Data extends Record<string, any>,
//   Computed extends RecordFunction,
//   Methods extends RecordFunction
// >(options: Vue<Data, Computed, Methods>): any;
