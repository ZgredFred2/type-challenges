// take args, append them, if return is primitive reutrn stack
type CurryingType<F> = F extends (args: infer A, ...rest: infer REST) => infer R
  ? REST["length"] extends 0
  ? F
  : (arg0: A) => CurryingType<(...args: REST) => R>
  : never;

declare function Currying<F>(fn: F): CurryingType<F>;

// declare function Currying<F>(fn: F): F extends (
//   args: infer A,
//   ...rest: infer REST
// ) => infer R
//   ? REST["length"] extends 0
//   ? F
//   : // is there way to make this work? I can put `(...args: REST) => asd` and its happy
//   (arg0: A) => typeof Currying<(...args: REST) => R>
//   : never;

// const curried3 = Currying(() => true);
// const curried5 = Currying((_a: string) => true);
// const curried4 = Currying((_a: string, _b: number) => true);
//
// const curried2 = Currying(
//   (
//     _a: string,
//     _b: number,
//     _c: boolean,
//     _d: boolean,
//     _e: boolean,
//     _f: string,
//     _g: boolean
//   ) => true
// );
// const curried1 = Currying((_a: string, _b: number, _c: boolean) => true);
