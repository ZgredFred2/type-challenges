type LookUp<U extends { type: string }, T> = U["type"] extends T ? U : never;

type LookUp<U extends { type: string }, T> = U extends any
  ? U["type"] extends T
  ? U
  : never
  : never;
interface Cat {
  type: "cat";
  breeds: "Abyssinian" | "Shorthair" | "Curl" | "Bengal";
}

interface Dog {
  type: "dog";
  breeds: "Hound" | "Brittany" | "Bulldog" | "Boxer";
  color: "brown" | "white" | "black";
}

type Animal = Cat | Dog;
type X11 = LookUp<Animal, "dog">;

type T112 = "dog" extends "dog" | "cat" ? true : false; // true
