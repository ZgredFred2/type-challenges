type PromiseReturnType<T> = T extends Promise<infer R> ? R : T;
type UnpromiseAll<T extends readonly any[]> = T extends [infer A, ...infer REST]
  ? [PromiseReturnType<A>, ...UnpromiseAll<REST>]
  : PromiseReturnType<T>;

// declare function PromiseAll<T extends readonly any[]>(
//   values: readonly [...T]
// ): Promise<UnpromiseAll<T>>; // UnpromiseAll<[...T]> also dont work

// type X2 = UnpromiseAll<[1, 2, 3]>;
// type X3 = UnpromiseAll<[Promise<number>, 3, string]>;
// type X4 = UnpromiseAll<[typeof Promise.resolve, 3, string]>;
// type R1 = PromiseReturnType<Promise<string>>;
// type R2 = PromiseReturnType<string>;

// const promiseAllTest2 = PromiseAll([1, 2, Promise.resolve(3), 2] as const);

// QUESTION: what exactly [...T] do compared to simply T ?
declare function PromiseAll<T extends readonly any[]>(
  values: readonly [...T]
): Promise<{ -readonly [K in keyof T]: PromiseReturnType<T[K]> }>;
