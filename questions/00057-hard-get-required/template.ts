type GetRequired<T> = {
  [P in keyof T as Omit<T, P> extends T ? never : P]: T[P];
};
type X5 = GetRequired<{ foo: number; bar?: string }>;

type T3 = { foo: number; bar: string } extends { foo: number } ? true : false; // true
type T4 = { foo?: number; bar: string } extends { foo: number } ? true : false; // false
type T5 = { bar: string } extends {} ? true : false; // true
