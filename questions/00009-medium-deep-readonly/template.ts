// type DeepReadonly<T> = T extends string | number | symbol | Function
//   ? T
//   : { readonly [P in keyof T]: DeepReadonly<T[P]> };

type DeepReadonly<T> = {
  readonly [K in keyof T]: T[K] extends Function ? T[K] : DeepReadonly<T[K]>;
};
